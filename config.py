import logging
import os

logger = logging.getLogger(__name__)


class DevelopmentConfig(object):
    BASE_DIR = os.path.dirname(os.path.realpath(__file__))
    SQLALCHEMY_DATABASE_URI = "sqlite:///{}/db.sqlite".format(BASE_DIR)

    HOST = "localhost"
    PORT = "5000"
    SECRET_KEY = "test"
    ENCRYPTION_KEY = "test"

    DEBUG = True
    SQLALCHEMY_TRACK_MODIFICATIONS = True

    # service code used for data blocking
    DATA_BLOCKING_CODE = "FTRS Data Blocking"

    # celery configuration parameters
    CELERY_BROKER_URL = "amqp://guest:guest@localhost:5672//amqp://localhost"
    CELERY_RESULT_BACKEND = "amqp://guest:guest@localhost:5672//amqp://localhost"
    CELERY_IMPORTS = ("src.tasks.plans")
