"""Entry point for celery worker instance

Example::
    $ celery -A celery_worker.celery worker --loglevel=DEBUG

"""
import config
from src import create_app
from src.celery_app import celery, configure_celery

app = create_app(config.DevelopmentConfig)
celery = configure_celery(app, celery)
