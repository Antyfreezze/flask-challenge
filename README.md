Prerequisites to run projects:
    installed python3.5 with development extensions
    installed rabbitmq-server
    created virtual environment with installed dependencies
    migrated database

Steps to run project:
    $ export FLASK_APP=att.py
    $ export FLASK_ENV=development
    $ python att.py
Run celery worker in another terminal window
    $ celery -A celery_worker.celery worker --loglevel=DEBUG

If there is needed to run celery task without flask application use next commands:
    $ export FLASK_APP=att.py
    $ export FLASK_ENV=development
    $ flask shell
    >>> from src.tasks.plans import query_subscription_plans
    >>> result = query_subscription_plans.delay(3, subscription_id=1)
    >>> result.wait()
