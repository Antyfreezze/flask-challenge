"""Plan related tasks"""
from celery.utils.log import get_task_logger
from src.celery_app import celery
from src.models.base import db
from src.models.versions_table import SubPlanVersions
from src.models.service_codes import Plan


log = get_task_logger(__name__)


@celery.task()
def query_subscription_plans(billing_cycle_id, subscription_id=None):
    """ Celery task function

    Args:
        billing_cycle_id (int): billing cycle id
        subscription_id (int, optional): subscription id or None

    Returns:
        list: list of tuples with data from data base

    Examples:
        [
            (
                10240,
                datetime.datetime(2019, 10, 1, 0, 0),
                datetime.datetime(2019, 11, 1, 0, 0)
            ),
            (
                10240,
                datetime.datetime(2019, 10, 1, 0, 0),
                datetime.datetime(2019, 11, 1, 0, 0)
            )
        ]

    There is ability to run this function from flask shell:
        $ flask shell
        >>> from src.tasks.plans import query_subscription_plans
        >>> result = query_subscription_plans.delay(3, subscription_id=1)
        >>> result.wait()

    """

    select_fields = [
        Plan.mb_available, SubPlanVersions.plan_start_date,
        SubPlanVersions.plan_end_date
    ]

    main_query = db.select(select_fields)
    query_billing_cycle_id = main_query.where(
        SubPlanVersions.billing_cycle_id == billing_cycle_id
    )
    query_stmt = query_billing_cycle_id.where(
        Plan.id == SubPlanVersions.plan_id
    )

    if subscription_id:
        query_stmt = query_stmt.where(
            SubPlanVersions.subscription_id == subscription_id
        )

    with db.engine.connect() as connection:
        result = connection.execute(query_stmt)
        return [tuple(row) for row in result]
