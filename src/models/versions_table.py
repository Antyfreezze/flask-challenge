"""  Model for subscription and plan verioning  """

from src.models.base import db


class SubPlanVersions(db.Model):
    """Model class to represent versions for subsription and plan record

    Note:

    """
    __tablename__ = "sub_plan_versions"

    id = db.Column(db.Integer, primary_key=True)
    version_date = db.Column(db.TIMESTAMP(timezone=True), nullable=False)
    subscription_id = db.Column(
        db.Integer, db.ForeignKey("subscriptions.id"), nullable=False
    )
    plan_id = db.Column(
        db.Integer, db.ForeignKey("plan.id"), nullable=False
    )
    billing_cycle_id = db.Column(
        db. Integer, db.ForeignKey("billing_cycles.id"), nullable=False
    )
    plan_start_date = db.Column(
        db.TIMESTAMP(timezone=True), db.ForeignKey("billing_cycles.start_date"), nullable=True
    )
    plan_end_date = db.Column(
        db.TIMESTAMP(timezone=True), db.ForeignKey("billing_cycles.end_date"), nullable=True
    )
