from http import HTTPStatus
from flask import abort
from flask import jsonify
from flask_restful import Resource
from src.tasks.plans import query_subscription_plans
from config import logger


class PlanQueryAPI(Resource):
    """Resource for lanching query_subscription_plans task"""

    def get(self, b_c_id, s_id=None):
        """Designed to simplify application health check"""
        logger.info("Launching query_subscription_plans task")
        task = query_subscription_plans.delay(b_c_id, s_id)
        query_result = task.wait()
        if not query_result:
            return abort(HTTPStatus.NOT_FOUND)
        return jsonify(query_result)
