"""Smoke resource for handling any smoke requests"""
from flask import jsonify
from flask_restful import Resource
from config import logger


class SmokeAPI(Resource):
    """Resource for smoke test"""

    def get(self):
        """Designed to simplify application health check"""
        logger.info("Smoke Resource was touched")
        return jsonify({"message": "Hello, World!"})
