"""empty message

Revision ID: c5a5e0a45eaa
Revises: d85f2051dea2
Create Date: 2020-07-01 19:21:17.415835

"""

from alembic import op
from datetime import datetime, timezone

import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'c5a5e0a45eaa'
down_revision = 'd85f2051dea2'
branch_labels = None
depends_on = None


def populate_sub_plan_versions(sub_plan_versions):
    op.bulk_insert(sub_plan_versions, [
        {
            'id': 1,
            'version_date': datetime(2019, 12, 1, tzinfo=timezone.utc),
            'plan_id': 3,
            'billing_cycle_id': 3,
            'plan_start_date': datetime(2019, 10, 1, tzinfo=timezone.utc),
            'plan_end_date': datetime(2019, 11, 1, tzinfo=timezone.utc),
            'subscription_id': 5
        },
        {
            'id': 2,
            'version_date': datetime(2019, 12, 2, tzinfo=timezone.utc),
            'plan_id': 2,
            'billing_cycle_id': 2,
            'plan_start_date': datetime(2019, 9, 1, tzinfo=timezone.utc),
            'plan_end_date': datetime(2019, 10, 1, tzinfo=timezone.utc),
            'subscription_id': 4
        },
        {
            'id': 3,
            'version_date': datetime(2019, 12, 3, tzinfo=timezone.utc),
            'plan_id': 3,
            'billing_cycle_id': 3,
            'plan_start_date': datetime(2019, 10, 1, tzinfo=timezone.utc),
            'plan_end_date': datetime(2019, 11, 1, tzinfo=timezone.utc),
            'subscription_id': 1
        },
        {
            'id': 4,
            'version_date': datetime(2019, 12, 4, tzinfo=timezone.utc),
            'plan_id': 1,
            'billing_cycle_id': 1,
            'plan_start_date': datetime(2019, 8, 1, tzinfo=timezone.utc),
            'plan_end_date': datetime(2019, 9, 1, tzinfo=timezone.utc),
            'subscription_id': 2
        },
        {
            'id': 5,
            'version_date': datetime(2019, 12, 5, tzinfo=timezone.utc),
            'plan_id': 2,
            'billing_cycle_id': 2,
            'plan_start_date': datetime(2019, 9, 1, tzinfo=timezone.utc),
            'plan_end_date': datetime(2019, 10, 1, tzinfo=timezone.utc),
            'subscription_id': 7
        },
        {
            'id': 6,
            'version_date': datetime(2019, 12, 6, tzinfo=timezone.utc),
            'plan_id': 1,
            'billing_cycle_id': 1,
            'plan_start_date': datetime(2019, 8, 1, tzinfo=timezone.utc),
            'plan_end_date': datetime(2019, 9, 1, tzinfo=timezone.utc),
            'subscription_id': 6
        },
        {
            'id': 7,
            'version_date': datetime(2019, 12, 7, tzinfo=timezone.utc),
            'plan_id': 2,
            'billing_cycle_id': 2,
            'plan_start_date': datetime(2019, 9, 1, tzinfo=timezone.utc),
            'plan_end_date': datetime(2019, 10, 1, tzinfo=timezone.utc),
            'subscription_id': 3
        },
        {
            'id': 8,
            'version_date': datetime(2019, 12, 8, tzinfo=timezone.utc),
            'plan_id': 1,
            'billing_cycle_id': 1,
            'plan_start_date': datetime(2019, 8, 1, tzinfo=timezone.utc),
            'plan_end_date': datetime(2019, 9, 1, tzinfo=timezone.utc),
            'subscription_id': 6
        },
        {
            'id': 9,
            'version_date': datetime(2019, 12, 9, tzinfo=timezone.utc),
            'plan_id': 2,
            'billing_cycle_id': 2,
            'plan_start_date': datetime(2019, 9, 1, tzinfo=timezone.utc),
            'plan_end_date': datetime(2019, 10, 1, tzinfo=timezone.utc),
            'subscription_id': 7
        },
        {
            'id': 10,
            'version_date': datetime(2019, 12, 10, tzinfo=timezone.utc),
            'plan_id': 3,
            'billing_cycle_id': 3,
            'plan_start_date': datetime(2019, 10, 1, tzinfo=timezone.utc),
            'plan_end_date': datetime(2019, 11, 1, tzinfo=timezone.utc),
            'subscription_id': 1
        },
        {
            'id': 11,
            'version_date': datetime(2019, 12, 11, tzinfo=timezone.utc),
            'plan_id': 1,
            'billing_cycle_id': 1,
            'plan_start_date': datetime(2019, 8, 1, tzinfo=timezone.utc),
            'plan_end_date': datetime(2019, 9, 1, tzinfo=timezone.utc),
            'subscription_id': 2
        },
        {
            'id': 12,
            'version_date': datetime(2019, 12, 12, tzinfo=timezone.utc),
            'plan_id': 2,
            'billing_cycle_id': 2,
            'plan_start_date': datetime(2019, 9, 1, tzinfo=timezone.utc),
            'plan_end_date': datetime(2019, 10, 1, tzinfo=timezone.utc),
            'subscription_id': 3
        },
        {
            'id': 13,
            'version_date': datetime(2019, 12, 13, tzinfo=timezone.utc),
            'plan_id': 3,
            'billing_cycle_id': 3,
            'plan_start_date': datetime(2019, 10, 1, tzinfo=timezone.utc),
            'plan_end_date': datetime(2019, 11, 1, tzinfo=timezone.utc),
            'subscription_id': 5
        },
        {
            'id': 14,
            'version_date': datetime(2019, 12, 14, tzinfo=timezone.utc),
            'plan_id': 2,
            'billing_cycle_id': 2,
            'plan_start_date': datetime(2019, 9, 1, tzinfo=timezone.utc),
            'plan_end_date': datetime(2019, 10, 1, tzinfo=timezone.utc),
            'subscription_id': 4
        }
    ])


def upgrade():
    sub_plan_versions = op.create_table('sub_plan_versions',
        sa.Column('id', sa.Integer(), nullable=False),
        sa.Column('version_date', sa.TIMESTAMP(timezone=True), nullable=False),
        sa.Column('subscription_id', sa.Integer(), nullable=False),
        sa.ForeignKeyConstraint(['subscription_id'], ['subscriptions.id']),
        sa.Column('plan_id', sa.Integer(), nullable=False),
        sa.ForeignKeyConstraint(['plan_id'], ['plans.id'], ),
        sa.Column('billing_cycle_id', sa.Integer(), nullable=False),
        sa.ForeignKeyConstraint(['billing_cycle_id'], ['billing_cycles.id'], ),
        sa.Column('plan_start_date', sa.TIMESTAMP(timezone=True), nullable=True),
        sa.ForeignKeyConstraint(['plan_start_date'], ['billing_cycles.start_date'], ),
        sa.Column('plan_end_date', sa.TIMESTAMP(timezone=True), nullable=True),
        sa.ForeignKeyConstraint(['plan_end_date'], ['billing_cycles.end_date'], ),
        sa.PrimaryKeyConstraint('id')
    )
    populate_sub_plan_versions(sub_plan_versions)


def downgrade():
    op.drop_table('sub_plan_versions')
